# webserver-automate
With Ajenti web panel configure a nginx Web Server. Also there is auto backup shell scripts.
I use generaly debian. You will find the bash scripts for debian.

For download with wget copy the codes below.

wget https://gitlab.com/bdogru/webserver-automate/raw/master/debian9-nginx-php7.2-mysql8-ajenti.sh

wget https://gitlab.com/bdogru/webserver-automate/raw/master/install_ghost_server_debian_9-php7.0.sh

wget https://gitlab.com/bdogru/webserver-automate/raw/master/Install_server_debian_9-php7.0.sh

wget https://gitlab.com/bdogru/webserver-automate/raw/master/mysql_backups.sh
